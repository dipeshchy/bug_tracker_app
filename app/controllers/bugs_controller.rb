class BugsController < ApplicationController
    before_action :authenticate_user!

    # method to list all bugs for a project
    def index
        @project = Project.find(params[:project_id])
    end

    # method to list all bugs for all projects
    def all_bugs
        @bugs = Bug.all
    end

    def new
        @project = Project.find(params[:project_id])
        @bug = @project.bugs.new
    end

    # method to create bug for project
    def create
        @project = Project.find(params[:project_id])
        @bug = @project.bugs.create(bug_params)
        flash[:success] = 'Bug added!'
        redirect_to project_bug_path(@project,@bug)
    end

    # method to show single bug
    def show
        @project = Project.find(params[:project_id])
        @bug = @project.bugs.find(params[:id])
    end

    # method to redirect to edit page
    def edit
        @project = Project.find(params[:project_id])
        @bug = @project.bugs.find(params[:id])
    end

    # method to update
    def update
        @project = Project.find(params[:project_id])
        @bug = @project.bugs.find(params[:id])
        if @bug.update(bug_params)
            redirect_to(project_bugs_path)
        else
          render('edit')
        end
    end

    # method to delete bug
    def destroy
        @project = Project.find(params[:project_id])
        @bug = @project.bugs.find(params[:id])
        @bug.destroy
        flash[:danger] = 'Bug deleted!'
        redirect_to project_bugs_path(@project)
    end

    private
    def bug_params
        params.require(:bug).permit(:title,:status,:description,:assigned_to)
    end                                          
end
