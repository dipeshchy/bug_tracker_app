class ProjectsController < ApplicationController

    before_action :authenticate_user!
    before_action :set_project, only: [:show, :edit, :update, :destroy]

    # method to show single project
    def show
    end
    
    # method to show all project
    def index
        @projects = Project.all
    end

    # method to redirect to create project form
    def new
        @project = Project.new
    end

    # method to create project
    def create
        @project = Project.new(project_params)
        if @project.save
            flash[:success] = 'Project Added Successfully!'
            redirect_to @project
        else
            render 'new'
        end
    end

    # method to redirect to edit page
    def edit
    end

    #method to delete project. It first finds project by id then deletes
    def destroy
        @project.destroy
        flash[:danger] = 'Project Deleted!'
        redirect_to projects_path
    end

    # method to update the project
    def update
        if @project.update(project_params)
            flash[:success] = 'Project Updated!'
            redirect_to @project
        else
            render 'edit'
        end
    end

    # method for security of input params
    private
    def project_params
        params.require(:project).permit(:title)
    end
    
    def set_project
        @project = Project.find(params[:id])
    end  


end
