class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  has_many :comments , dependent: :destroy
  devise :invitable, :database_authenticatable,
         :recoverable, :rememberable, :validatable, :confirmable
         
end
