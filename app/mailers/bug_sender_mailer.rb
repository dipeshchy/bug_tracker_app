class BugSenderMailer < ApplicationMailer
    default from: 'chaudharydipesh627@gmail.com'

    CONTACT_EMAIL = "brodanakarmi123@gmail.com"
  
    def submission
      @bugs = Bug.all
      mail(to: CONTACT_EMAIL, subject: 'Bugs Report')
    end
end
