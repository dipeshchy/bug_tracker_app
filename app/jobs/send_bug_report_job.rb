class SendBugReportJob < ApplicationJob
  queue_as :default

  def perform
    BugSenderMailer.submission.deliver
  end
end
