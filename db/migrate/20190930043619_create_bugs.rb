class CreateBugs < ActiveRecord::Migration[6.0]
  def change
    create_table :bugs do |t|
      t.string :title
      t.string :status
      t.text :description
      t.string :assigned_to
      t.references :project, null: false, foreign_key: true

      t.timestamps
    end
  end
end
