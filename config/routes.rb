Rails.application.routes.draw do

  root 'welcome#index'

  resources :projects do 
    resources :bugs do 
      resources :comments
    end
  end

  get "all_bugs",to: "bugs#all_bugs",as:"all_bugs"

  devise_for :users , except: :create
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
